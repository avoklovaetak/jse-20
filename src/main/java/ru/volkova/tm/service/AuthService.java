package ru.volkova.tm.service;

import ru.volkova.tm.api.service.IAuthService;
import ru.volkova.tm.api.service.IUserService;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.auth.AuthNoException;
import ru.volkova.tm.exception.empty.EmptyLoginException;
import ru.volkova.tm.exception.empty.EmptyPasswordException;
import ru.volkova.tm.exception.entity.UserNotFoundException;
import ru.volkova.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void setCurrentUserId(User user) {
        userId = user.getId();
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        setCurrentUserId(user);
        final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
    }

    public void registry(final String login, final String password, final String email) {
        userService.create(login, password, email);
    }

}
