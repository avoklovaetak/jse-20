package ru.volkova.tm.service;

import ru.volkova.tm.api.repository.IOwnerRepository;
import ru.volkova.tm.api.service.IOwnerService;
import ru.volkova.tm.entity.AbstractOwnerEntity;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.empty.EmptyIdException;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.system.IndexIncorrectException;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity>
        extends AbstractService<E> implements IOwnerService<E> {

    protected final IOwnerRepository<E> ownerRepository;

    protected AbstractOwnerService(IOwnerRepository<E> repository) {
        super(repository);
        this.ownerRepository = repository;
    }

    @Override
    public List<E> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return ownerRepository.findAll(userId);
    }

    @Override
    public E findById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.findById(userId, id);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        ownerRepository.clear(userId);
    }

    @Override
    public E removeById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return ownerRepository.removeById(userId, id);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (comparator == null) return null;
        return ownerRepository.findAll(userId, comparator);
    }

    @Override
    public E findOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return ownerRepository.findOneByName(userId, name);
    }

    @Override
    public E removeOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return ownerRepository.removeOneByName(userId, name);
    }

    @Override
    public E removeOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return ownerRepository.removeOneByIndex(userId, index);
    }

    @Override
    public E findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        return ownerRepository.findOneByIndex(userId, index);
    }

    @Override
    public E updateOneByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByIndex(userId, index);
        if (entity == null) return null;
        entity .setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E updateOneById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E startOneById(final String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E startOneByName(final String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByName(userId, name);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E startOneByIndex(final String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final E entity = findOneByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E finishOneById(final String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E finishOneByName(final String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByName(userId, name);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E finishOneByIndex(final String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final E entity = findOneByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E changeOneStatusById(final String userId, String id, Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS) entity.setDateStart(new Date());
        if (status == Status.COMPLETE) entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E changeOneStatusByName(final String userId, String name, Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final E entity = findOneByName(userId, name);
        if (entity == null) return null;
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS) entity.setDateStart(new Date());
        if (status == Status.COMPLETE) entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E changeOneStatusByIndex(final String userId, Integer index, Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        final E entity = findOneByIndex(userId, index);
        if (entity == null) return null;
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS) entity.setDateStart(new Date());
        if (status == Status.COMPLETE) entity.setDateFinish(new Date());
        return entity;
    }

}
