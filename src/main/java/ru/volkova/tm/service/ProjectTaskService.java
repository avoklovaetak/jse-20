package ru.volkova.tm.service;

import ru.volkova.tm.api.repository.IProjectRepository;
import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.api.service.IProjectTaskService;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.exception.auth.AccessDeniedException;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task bindTaskByProjectId(final String userId, final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepository.bindTaskByProjectId(userId, projectId,taskId);
    }

    @Override
    public Task unbindTaskByProjectId(final String userId, final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepository.unbindTaskByProjectId(userId, projectId,taskId);
    }

    @Override
    public Project removeProjectById(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        taskRepository.removeAllByProjectId(userId, projectId);
        return projectRepository.removeById(userId, projectId);
    }

}
