package ru.volkova.tm.service;

import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.api.service.ITaskService;
import ru.volkova.tm.entity.Task;

public final class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final String userId, final String name, final String description) {
        return taskRepository.add(userId, name, description);
    }

}
