package ru.volkova.tm.service;

import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.api.IService;
import ru.volkova.tm.entity.AbstractEntity;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public E add(final E entity) {
        if (entity == null) throw new ObjectNotFoundException();
        return repository.add(entity);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) throw new ObjectNotFoundException();
        repository.remove(entity);
    }

}
