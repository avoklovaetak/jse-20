package ru.volkova.tm.service;

import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.api.service.IUserService;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.auth.EmailExistsException;
import ru.volkova.tm.exception.auth.LoginExistsException;
import ru.volkova.tm.exception.empty.*;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.UserNotFoundException;
import ru.volkova.tm.util.HashUtil;

import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public void remove(final User user) {
        if (user == null) throw new ObjectNotFoundException();
        userRepository.remove(user);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isLoginExists(final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isEmailExists(final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public void removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        final User user = create(login, password);
        if (user == null) throw new UserNotFoundException();
        user.setEmail(email);
        return user;
    }

    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        if (user == null) throw new UserNotFoundException();
        user.setRole(role);
        return user;
    }

    public User setPassword (final String userId, final String password) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(userId);
        if (user == null) throw new UserNotFoundException();
        final String hash = HashUtil.salt(password);
        user.setPasswordHash(hash);
        return user;
    }

    public User updateUser(
            final String userId,
            final String firstName,
            final String secondName,
            final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final User user = findById(userId);
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setMiddleName(middleName);
        return user;
    }

}
