package ru.volkova.tm.command.authorization;

import ru.volkova.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-registry";
    }

    @Override
    public String description() {
        return "registry new user in system";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRATION]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
    }

}
