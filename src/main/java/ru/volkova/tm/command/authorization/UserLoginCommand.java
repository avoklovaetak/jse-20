package ru.volkova.tm.command.authorization;

import ru.volkova.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-login";
    }

    @Override
    public String description() {
        return "to enter in application";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

}
