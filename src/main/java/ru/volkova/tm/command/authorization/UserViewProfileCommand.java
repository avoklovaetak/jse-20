package ru.volkova.tm.command.authorization;

import ru.volkova.tm.entity.User;

public class UserViewProfileCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-view-profile";
    }

    @Override
    public String description() {
        return "view user profile";
    }

    @Override
    public void execute() {
        System.out.println("[VIEW PROFILE]");
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getSecondName());
        System.out.println("MIDDLE NAME" + user.getMiddleName());
    }

}
