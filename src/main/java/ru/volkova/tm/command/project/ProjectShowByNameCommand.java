package ru.volkova.tm.command.project;

import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.util.TerminalUtil;

public class ProjectShowByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-show-by-name";
    }

    @Override
    public String description() {
        return "show project by name";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String userId = serviceLocator.getAuthService().getUserId();
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
