package ru.volkova.tm.command.project;

import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-change-status-by-index";
    }

    @Override
    public String description() {
        return "change project status by index";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER ID");
        final Integer index = TerminalUtil.nextNumber()-1;
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = serviceLocator.getProjectService()
                .changeOneStatusByIndex(userId, index, status);
        if (project == null) throw new ProjectNotFoundException();
    }

}
