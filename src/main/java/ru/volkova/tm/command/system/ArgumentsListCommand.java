package ru.volkova.tm.command.system;

import ru.volkova.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentsListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "arguments-list";
    }

    @Override
    public String description() {
        return "show program arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command: commands) {
            final String arg = command.arg();
            if (arg == null) continue;
            System.out.println(arg);
        }
    }

}
