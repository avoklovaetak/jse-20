package ru.volkova.tm.command.system;

import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.util.NumberUtil;

public class SystemInfoCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "system-info";
    }

    @Override
    public String description() {
        return "show system info";
    }

    @Override
    public void execute() {
        System.out.println("[SYSTEM INFO]");
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.format(freeMemory));
        final Long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = isMaxMemory ? "no limit" : NumberUtil.format(maxMemory);
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long memoryTotal = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.format(memoryTotal));
        final long usedMemory = memoryTotal - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.format(usedMemory));
    }

}
