package ru.volkova.tm.command.bonds;

import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.util.TerminalUtil;

public class TaskBindByProjectIdCommand extends AbstractProjectTaskClass {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-bind-by-project-id";
    }

    @Override
    public String description() {
        return "task bind to project by project id";
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getProjectTaskService()
                .bindTaskByProjectId(userId, projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
    }

}
