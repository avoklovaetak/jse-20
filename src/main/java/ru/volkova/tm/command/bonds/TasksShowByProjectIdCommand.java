package ru.volkova.tm.command.bonds;

import ru.volkova.tm.entity.Task;
import ru.volkova.tm.util.TerminalUtil;

import java.util.List;

public class TasksShowByProjectIdCommand extends AbstractProjectTaskClass {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "tasks-show-by-project-id";
    }

    @Override
    public String description() {
        return "show all tasks of project by project id";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST OF PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final List<Task> tasks = serviceLocator.getProjectTaskService()
                .findAllTasksByProjectId(userId, projectId);
        int index = 1;
        for (final Task task: tasks) {
            System.out.println((index + ". " + task));
            index++;
        }
    }

}

