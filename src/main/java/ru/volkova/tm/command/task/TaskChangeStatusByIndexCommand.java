package ru.volkova.tm.command.task;

import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-change-status-by-index";
    }

    @Override
    public String description() {
        return "change task status by index";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER ID");
        final Integer index = TerminalUtil.nextNumber()-1;
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final String userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().changeOneStatusByIndex(userId, index, status);
        if (task == null) throw new TaskNotFoundException();
    }

}

