package ru.volkova.tm.command.task;

import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.util.TerminalUtil;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-start-by-id";
    }

    @Override
    public String description() {
        return "start task by id";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final Task task = serviceLocator.getTaskService().startOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
    }

}
