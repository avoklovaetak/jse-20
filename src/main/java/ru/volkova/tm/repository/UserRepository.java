package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.UserNotFoundException;

import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public List<User> findAll() {
        return entities;
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new ObjectNotFoundException();
        for (final User user: entities) {
            if (user == null) continue;
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        if (user == null) throw new ObjectNotFoundException();
        entities.remove(user);
        return user;
    }

    @Override
    public void remove(final User user) {
        entities.remove(user);
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user: entities) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user: entities) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public void removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        remove(user);
    }

}
