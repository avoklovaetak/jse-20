package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.IOwnerRepository;
import ru.volkova.tm.entity.AbstractOwnerEntity;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity>
        extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public List<E> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final List<E> curEntities = new ArrayList<>();
        for (final E entity: entities){
            if (userId.equals(entity.getUserId())) curEntities.add(entity);
        }
        return curEntities;
    }

    @Override
    public E findById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new ObjectNotFoundException();
        for (final E entity: entities) {
            if (entity == null) continue;
            if (id.equals(entity.getId())
                    && userId.equals(entity.getUserId())) return entity;
        }
        return null;
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        entities.clear();
    }

    @Override
    public E removeById(final String userId, final String id) {
        final E entity = findById(userId, id);
        if (entity == null) throw new ObjectNotFoundException();
        entities.remove(entity);
        return entity;
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final List<E> curEntities = new ArrayList<>();
        for (final E entity: entities){
            if (userId.equals(entity.getUserId())) curEntities.add(entity);
        }
        curEntities.sort(comparator);
        return curEntities;
    }

    @Override
    public E findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        final E entity = entities.get(index);
        if (userId.equals(entity.getUserId())) return entity;
        return null;
    }

    @Override
    public E findOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        for (final E entity: entities) {
            if (name.equals(entity.getName())
                    && userId.equals(entity.getUserId())) return entity;
        }
        return null;
    }

    @Override
    public E removeOneByIndex(final String userId, final Integer index) {
        final E entity = findOneByIndex(userId, index);
        if (entity == null) throw new ObjectNotFoundException();
        remove(entity);
        return entity;
    }

    @Override
    public E removeOneByName(final String userId, final String name) {
        final E entity = findOneByName(userId, name);
        if (entity == null) throw new ProjectNotFoundException();
        remove(entity);
        return entity;
    }

}
