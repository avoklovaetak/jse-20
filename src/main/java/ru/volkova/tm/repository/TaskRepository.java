package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.empty.EmptyNameException;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public Task add(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        add(task);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> projectTasks = new ArrayList<>();
        for (final Task task: entities){
            if (projectId.equals(task.getProjectId())
                    && userId.equals(task.getUserId())) projectTasks.add(task);
        }
        return projectTasks;
    }

    @Override
    public void removeAllByProjectId(final String userId, final String projectId) {
        entities.removeIf(task -> projectId.equals(task.getProjectId())
                && userId.equals(task.getUserId()));
    }

    @Override
    public Task bindTaskByProjectId(final String userId, final String projectId, final String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskByProjectId(final String userId, final String projectId, final String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(null);
        return task;
    }

}
