package ru.volkova.tm.repository;

import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public E add(final E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }

}
