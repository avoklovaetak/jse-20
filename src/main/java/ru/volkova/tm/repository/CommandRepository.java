package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.ICommandRepository;
import ru.volkova.tm.command.AbstractCommand;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    public void add(AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

    public AbstractCommand getCommandByArg(String name) {
        return commands.get(name);
    }

}
