package ru.volkova.tm.entity;

import ru.volkova.tm.api.entity.IWBS;

public final class Project extends AbstractOwnerEntity implements IWBS {

    @Override
    public String toString() {
        return id + ": " + getName() + "; " +
                "created: " + getCreated() +
                "started: " + getDateStart();
    }

}
