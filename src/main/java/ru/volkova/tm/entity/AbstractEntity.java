package ru.volkova.tm.entity;

import java.io.Serializable;
import java.util.UUID;

public abstract class AbstractEntity implements Serializable {

    public String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

}
