package ru.volkova.tm.exception.entity;

import ru.volkova.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found...");
    }

}
