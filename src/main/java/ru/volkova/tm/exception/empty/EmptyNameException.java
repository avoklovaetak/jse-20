package ru.volkova.tm.exception.empty;

import ru.volkova.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException{

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
