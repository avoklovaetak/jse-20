package ru.volkova.tm.exception.empty;

import ru.volkova.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Empty password exception...");
    }

}
