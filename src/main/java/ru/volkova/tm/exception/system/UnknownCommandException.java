package ru.volkova.tm.exception.system;

import ru.volkova.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(String command) {
        super("Error! Unknown ``" + command + "`` command...");
    }

}
