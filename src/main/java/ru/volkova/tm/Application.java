package ru.volkova.tm;

import ru.volkova.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }
    
}
