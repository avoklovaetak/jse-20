package ru.volkova.tm.api;

import ru.volkova.tm.entity.AbstractEntity;

public interface IService <E extends AbstractEntity> extends IRepository<E> {

}
