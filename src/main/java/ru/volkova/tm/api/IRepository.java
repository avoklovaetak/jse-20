package ru.volkova.tm.api;

import ru.volkova.tm.entity.AbstractEntity;

public interface IRepository <E extends AbstractEntity> {

    E add(E entity);

    void remove(E entity);

}
