package ru.volkova.tm.api.service;

import ru.volkova.tm.api.repository.IOwnerRepository;
import ru.volkova.tm.entity.AbstractOwnerEntity;
import ru.volkova.tm.enumerated.Status;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IOwnerRepository<E> {
    List<E> findAll(String userId, Comparator<E> comparator);

    E findOneByIndex(String userId, Integer index);

    E findOneByName(String userId, String name);

    E removeOneByIndex(String userId, Integer index);

    E removeOneByName(String userId, String name);

    E updateOneById(String userId, String id, String name, String description);

    E updateOneByIndex(String userId, Integer index, String name, String description);

    E startOneById(String userId, String id);

    E startOneByName(String userId, String name);

    E startOneByIndex(String userId, Integer index);

    E finishOneById(String userId, String id);

    E finishOneByName(String userId, String name);

    E finishOneByIndex(String userId, Integer index);

    E changeOneStatusById(String userId, String id, Status status);

    E changeOneStatusByName(String userId, String name, Status status);

    E changeOneStatusByIndex(String userId, Integer index, Status status);

}
