package ru.volkova.tm.api.service;

import ru.volkova.tm.entity.Project;

public interface IProjectService extends IOwnerService<Project> {

    Project add(String userId, String name, String description);

}
