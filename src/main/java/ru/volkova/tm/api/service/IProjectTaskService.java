package ru.volkova.tm.api.service;

import ru.volkova.tm.entity.Project;
import ru.volkova.tm.entity.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTasksByProjectId(String userId, String projectId);

    Task bindTaskByProjectId(String userId, String projectId, String taskId);

    Task unbindTaskByProjectId(String userId, String projectId, String taskId);

    Project removeProjectById(String userId, String id);

}
