package ru.volkova.tm.api.service;

import ru.volkova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<AbstractCommand> getArguments();

    Collection<AbstractCommand> getCommands();

    void add(AbstractCommand command);

}
