package ru.volkova.tm.api.service;

import ru.volkova.tm.entity.Task;

public interface ITaskService extends IOwnerService<Task> {

    Task add(String userId, String name, String description);

}
