package ru.volkova.tm.api.repository;

import java.util.Comparator;
import java.util.List;
import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.AbstractOwnerEntity;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    List<E> findAll(String userId);

    E findById(String userId, String id);

    void clear(String userId);

    E removeById(String userId, String id);

    List<E> findAll(String userId, Comparator<E> comparator);

    E findOneByIndex(String userId, Integer index);

    E findOneByName(String userId, String name);

    E removeOneByIndex(String userId, Integer index);

    E removeOneByName(String userId, String name);

}
