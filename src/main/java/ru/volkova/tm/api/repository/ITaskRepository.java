package ru.volkova.tm.api.repository;

import ru.volkova.tm.entity.Task;
import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    Task add(String userId, String name, String description);

    List<Task> findAllByProjectId(String userId, String projectId);

    void removeAllByProjectId(String userId, String projectId);

    Task bindTaskByProjectId(String userId, String projectId, String taskId);

    Task unbindTaskByProjectId(String userId, String projectId, String taskId);

}
