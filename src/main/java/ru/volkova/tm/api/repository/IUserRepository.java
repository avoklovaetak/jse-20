package ru.volkova.tm.api.repository;

import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    List<User> findAll();

    User findById(String id);

    void clear();

    User removeById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    void removeByLogin(String login);

}
