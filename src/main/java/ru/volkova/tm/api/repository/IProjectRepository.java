package ru.volkova.tm.api.repository;

import ru.volkova.tm.entity.Project;

public interface IProjectRepository extends IOwnerRepository<Project> {

    Project add(String userId, String name, String description);

}
